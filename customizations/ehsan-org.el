(require 'org)
(defun org-clocking-buffer (&rest _))
;; Fancy bullets and TODO
;;-----------------------------------------------------;;
(use-package org-bullets
:ensure t
:config
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(setq org-default-notes-file "~/org/tasks.org")
(add-to-list 'org-capture-templates
             '("t" "Personal Task"  entry
               (file org-default-notes-file)
               "* TODO %?" :empty-lines 1))

(add-to-list 'org-capture-templates
             '("w" "Work-related Task"  entry
               (file "~/org/work.org")
               "* TODO %?" :empty-lines 1))

(global-set-key (kbd "C-c o") 
                (lambda () (interactive) (find-file "~/org/organizer.org")))

(setq org-refile-targets '((org-agenda-files . (:maxlevel . 6))))

;;-----------------------------------------------------;;
(require 'ox-latex)
(setq org-latex-create-formula-image-program 'dvisvgm)
(setq org-format-latex-options (plist-put org-format-latex-options :scale 2.0))
(use-package org-ref
    :custom
    (org-ref-default-bibliography "~/zotero.bib"))
;;
(setq org-highlight-latex-and-related '(latex))


(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
;; ** <<APS journals>>
(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes '("revtex4-2"
				  "\\documentclass{revtex4-2}
 [NO-DEFAULT-PACKAGES]
 [PACKAGES]
 [EXTRA]"
				  ("\\section{%s}" . "\\section*{%s}")
				  ("\\subsection{%s}" . "\\subsection*{%s}")
				  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
				  ("\\paragraph{%s}" . "\\paragraph*{%s}")
				  ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

(with-eval-after-load 'org
  (setq org-display-inline-images nil)
  (setq org-redisplay-inline-images nil)
  ;(setq org-startup-with-inline-images "inlineimages")
  (setq org-hide-emphasis-markers nil)
  (add-to-list 'org-file-apps '("\\.pdf\\'" . emacs))
  (setq org-confirm-elisp-link-function nil))
;  (setq org-link-frame-setup '((file . find-file))))


(add-to-list 'load-path "~/.emacs.d/elpa/emacs-reveal")
(require 'emacs-reveal)
