;; appearance settings

;;----------------------------------------------------------------------;;
(setq default-frame-alist
      (append (list '(width  . 142) '(height . 40)
                    '(vertical-scroll-bars . nil)
                    '(internal-border-width . 24))))
                    ;'(font . "Roboto Mono Light Italic 11"))))
(set-frame-parameter (selected-frame)
                     'internal-border-width 24)
;;----------------------------------------------------------------------;;

;;----------------------------------------------------------------------;;
(global-visual-line-mode 1)
(global-display-line-numbers-mode t)
(setq display-line-numbers-type 'relative)
(set-default 'truncate-lines t)
; To be able scroll horizontally in pdf-view:
(add-hook 'pdf-view-mode-hook (lambda () (display-line-numbers-mode -1)))
;;----------------------------------------------------------------------;;

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/elegant-emacs")

(defun set-bidi-env ()
  "interactive"
  (setq bidi-paragraph-direction 'nil))
(add-hook 'org-mode-hook 'set-bidi-env)
;; The rest of seetings are taken from the elegence theme
;; Font and frame size
;(set-face-font 'default "Vazir Light 11")
(set-fontset-font
   "fontset-default"
   (cons (decode-char 'ucs #x0600) (decode-char 'ucs #x06ff)) ; arabic
   "Vazir Light 11")
;;(set-face-font 'default "Source Code Pro Light 12")
;;(set-face-font 'default "Roboto Mono Thin 11")
;;(set-face-font 'default "Input Mono Thin 11")
;;(set-face-font 'default "Iosevka SS02 Extralight 13")
(set-face-font 'default "Iosevka SS09 Light 13")
;; Line spacing, can be 0 for code and 1 or 2 for text


;(setq-default line-spacing 0)

;; Underline line at descent position, not baseline position
(setq x-underline-at-descent-line t)

;; No ugly button for checkboxes
(setq widget-image-enable nil)

;; Line cursor and no blink
;(set-default 'cursor-type  '(bar . 1))
;(blink-cursor-mode 0)

;; Paren mode is part of the theme
(show-paren-mode t)

;; No fringe but nice glyphs for truncated and wrapped lines
(fringe-mode '(0 . 0))
(defface fallback '((t :family "Fira Code Light"
                       :inherit 'face-faded)) "Fallback")
(set-display-table-slot standard-display-table 'truncation
                        (make-glyph-code ?… 'fallback))
(set-display-table-slot standard-display-table 'wrap
                        (make-glyph-code ?↩ 'fallback))
(set-display-table-slot standard-display-table 'selective-display
                        (string-to-vector " …"))

;; Vertical window divider
(setq window-divider-default-right-width 3)
(setq window-divider-default-places 'right-only)
(window-divider-mode)

